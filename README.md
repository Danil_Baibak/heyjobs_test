# The tasks:

Write a program (not a Python notebook) that trains a linear regression for the training file, and predicts the number of applications for the entries in the test file. The inputs are: the Job type 
(represented by an integer here, but it is a categorical variable), the salary per hour, the city, and the number of hours each week. The target is the number of applications.

## Installation

For installation please use [conda](http://conda.pydata.org/docs/using/index.html). 

Setup project:
```sh
make init
```
After the installation was done successfully, activate your enviroment:
```sh
source activate heyjobs
```

## Usage

There is file `script.py`, that can built a model and does predictions. There're two possible arguments for the script:
```
optional arguments:
  -h, --help         show this help message and exit
  --train TRAIN      Data set for training
  --predict PREDICT  Data set for prediction
```
(**Important!** For using the script you have to save files (for instance `training.csv`) into the _data_ folder.)

### Train new model:
```
python script.py --train='data/training.csv'
```
The output would be path to the saved model.

### Make rpediction:
```
python script.py --predict='data/test.csv'
```
The output would be path to the file with predictions. By default the script uses the latest saved model.