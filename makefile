init:
	conda env create -f environment.yml
	mkdir -p data && mkdir -p models && mkdir -p predictions
