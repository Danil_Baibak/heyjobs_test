#!/usr/bin/python

import glob
import os
import sys

import pandas as pd
import numpy as np
import argparse
import time

# custom helpers
from helpers.pipeline import LabelBinarizerPipelineFriendly, ItemSelector

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Ridge
from sklearn.externals import joblib

seed = 42
np.random.seed(seed)

# set root path for the project
os.chdir(os.path.dirname(os.path.abspath(__file__)))


def get_arguments():
    """
    Get arguments of the command
    :return: list of the arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', help='Data set for training')
    parser.add_argument('--predict', help='Data set for prediction')

    return parser.parse_args()


def is_file(file_path):
    """
    Check if file exists
    """
    if not os.path.isfile(file_path):
        print("The file {} doesn't exist".format(file_path))
        exit(2)


class Model(object):
    def __init__(self):
        self.model = None

        if os.listdir('models/'):
            # get last model
            list_of_files = glob.glob('models/*.pkl')
            self.model = joblib.load(max(list_of_files, key=os.path.getctime))

    def train_model(self, file_path):
        """
        Train a model according the new dataset and save it
        :param file_path: path to the train dataset
        :return: path to the new model
        """
        df_train = pd.read_csv(file_path, dtype={'job_type': 'category', 'hours': 'category', 'city': 'category'})

        self.model = Pipeline([
            ('feature_processing', FeatureUnion(transformer_list=[
                ('numerical_features', Pipeline([
                    ('selecting', ItemSelector(key=['salary'])),
                    ('std_scaler', StandardScaler())
                ])),
                ('payment', Pipeline([
                    ('selecting', ItemSelector(key='job_type')),
                    ('label_binarizer', LabelBinarizerPipelineFriendly())
                ])),
                ('platform', Pipeline([
                    ('selecting', ItemSelector(key='hours')),
                    ('label_binarizer', LabelBinarizerPipelineFriendly())
                ])),
                ('transmission', Pipeline([
                    ('selecting', ItemSelector(key='city')),
                    ('label_binarizer', LabelBinarizerPipelineFriendly())
                ]))
            ])),
            ('ridge', Ridge(random_state=seed))
        ])

        # trina new model
        self.model.fit(df_train, df_train['applications'])

        # TODO - evaluate model, do log metrics or send notification if the model is poor

        # save current model
        model_path = 'models/{}.pkl'.format(time.strftime("%Y%m%d_%H%M%S"))
        joblib.dump(self.model, model_path)

        return model_path

    def make_prediction(self, file_path):
        """
        make a prediction and save it to file
        :param file_path: path to the test dataset
        :return: path to the file with predictions
        """
        if not self.model:
            print('There is no model yet! Train model before.')
            exit(2)

        df_test = pd.read_csv(file_path, dtype={'job_type': 'category', 'hours': 'category', 'city': 'category'})
        predictions = self.model.predict(df_test)
        df_predict = pd.DataFrame({'job_id': df_test['job_id'], 'applications': predictions}, columns=['job_id', 'applications'])

        # save predictions
        predictions_path = 'predictions/{}.csv'.format(time.strftime("%Y%m%d_%H%M%S"))
        df_predict.to_csv(predictions_path, index=False)

        return predictions_path


if __name__ == '__main__':
    args = get_arguments()
    model = Model()

    # train model and make prediction
    if args.train and args.predict:
        is_file(args.train)
        is_file(args.predict)

        model_path = model.train_model(args.train)
        print('New model was saved: {}'.format(model_path))

        predictions_path = model.make_prediction(args.predict)
        print('You can find prediction: {}'.format(predictions_path))
    elif args.train:
        # just train new model
        is_file(args.train)

        model_path = model.train_model(args.train)
        print('New model was saved: {}'.format(model_path))
    elif args.predict:
        # just make prediction
        is_file(args.predict)

        predictions_path = model.make_prediction(args.predict)
        print('You can find prediction: {}'.format(predictions_path))
    else:
        print('No arguments were passed')
        sys.exit(2)
